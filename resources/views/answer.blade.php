@extends('layouts.app')

@section('content')
    <div class="container">

        {{--Question--}}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                        Question - By {{ $question->user->name }}
                    </div>
                    <div class="panel-body">
                        {{$question->question_text}} <br>
                        @if($question->image_path == null)
                        @else
                            <img src="{{ asset($question->image_path) }}"/>
                        @endif
                        @if($question->tags->isEmpty())
                        @else
                            <h4>Tags:</h4>
                            <ul>
                                @foreach($question->tags as $tag)
                                    <li style="display: inline;">{{ $tag->name }}.</li>
                                @endforeach
                            </ul>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        {{--Answer Lists--}}
        @if($answers == null)
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-info">

                        <div class="panel-body">
                            No Answer now. You have an idea? Add it below ! :)
                        </div>
                    </div>
                </div>
            </div>
        @else
            {{--*/ $i = 1 /*--}}
            @foreach($answers as $answer)
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                                Answer #{{$i}} - By {{ $answer->name }}
                                <a href="{{ url('vote/answer', [$answer->id]) }}">
                                    <p class="pull-right">
                                        <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
                                        Votes: {{ $answer->votes }}
                                    </p>
                                </a>
                            </div>

                            <div class="panel-body">
                                {{$answer->answer_text}} <br>
                                @if($answer->image_path == null)
                                @else
                                    <img src="{{ asset($answer->image_path) }}"/>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                {{--*/ $i++ /*--}}
            @endforeach
        @endif

        {{--Answer Box--}}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Answer this!</div>

                    <div class="panel-body">
                        <form role="form" action="{{ url('question/addanswer', [$question->id]) }}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <textarea class="form-control" rows="5" id="answertext" name="answertext"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="tag">Select Image:</label>
                                <input type="file" name="image" id="image" accept="image/*"/>
                            </div>
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
