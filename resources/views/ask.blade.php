@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Ask a question</div>

                    <div class="panel-body">
                        <form role="form">
                            <div class="form-group">
                                <label for="question">Question</label>
                                <textarea class="form-control" rows="5" id="askquestion" name="askquestion"></textarea>
                            </div>
                            <button type="submit" class="btn btn-default btn-lg">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
