@extends('layouts.app')

@section('content')
<div class="container">
    {{--*/ $i = 1 /*--}}
    @foreach($questions as $question)
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                    Question
                    <a href="{{ action('HomeController@answerView', ['id' => $question->id]) }}"  style="color: #fff" >
                        <p class="pull-right">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Answers
                        </p>
                    </a>
                </div>

                <div class="panel-body">
                    {{$question->question_text}}
                    <p><b>By</b> {{ $question->user->name }}</p>
                </div>
            </div>
        </div>
    </div>
    {{--*/ $i++ /*--}}
    @endforeach

    {!! $questions->links() !!}

</div>
@endsection
