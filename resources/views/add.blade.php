@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Ask friend a question
                    </div>

                    <div class="panel-body">
                        <form role="form" action="{{ url('/add/question') }}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="question">Question</label>
                                <textarea class="form-control" rows="5" id="questiontext" name="questiontext"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="tag">Tag:</label>
                                {!! Form::select('tags[]', $tags, null, ['class' => 'form-control', 'multiple']) !!}
                            </div>

                            <div class="form-group">
                                <label for="tag">Select Image:</label>
                                <input type="file" name="image" id="image" accept="image/*"/>
                            </div>
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
