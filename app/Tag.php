<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name',
    ];

    /*
     * Get the articles associated with tags
     */
    public function articles(){
        return $this->belongsToMany('App\Question');
    }
}
