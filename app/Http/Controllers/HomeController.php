<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Image;
use DB;
use App\Question;
use App\Answer;
use App\Tag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::simplePaginate(6);
//      $questions = Question::orderBy('created_at', 'desc')->simplePaginate(6);
        return view('newsfeed', compact('questions'));
    }


    /**
     * Add question view
     */
    public function addView()
    {
        $tags = Tag::lists('name', 'id');
        return view('add', compact('tags'));
    }

    /**
     * Add question if can't find one (Ask friend module)
     */
    public function addQuestion(Request $request)
    {
        $questionText = $request->get('questiontext');
        

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename  = "question_" . time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('img/question/' . $filename);

            Image::make($image->getRealPath())->resize(472, 294)->save($path);
            $question = Auth::user()->questions()->create(array(
                'question_text' => $questionText,
                'image_path' =>  'img/question/'.$filename,
            ));
            $question->tags()->attach($request->input('tags'));

        }else{
            $question = Auth::user()->questions()->create(array(
                'question_text' => $questionText,
            ));
            $question->tags()->attach($request->input('tags'));
        }

        return redirect('/');
    }

    /**
     * Answer View
     */

    public function answerView($id){

        $question = Question::findOrFail($id);
        $answers = DB::table('answers')
            ->join('users', 'users.id', '=', 'answers.user_id')
            ->where('questions_id', '=', $id)
            ->get();
        return view('answer', compact('question', 'answers'));

    }

    /**
     * Andd answer to the question
     */
    public function addAnswer(Request $request, $id){
        $answer = $request->get('answertext');
        $question = Question::findOrFail($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename  = "answer_" . time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('img/answer/' . $filename);

            Image::make($image->getRealPath())->resize(472, 294)->save($path);
            $answer_obj = new Answer();
            $answer_obj->answer_text = $answer;
            $answer_obj->image_path = 'img/answer/'.$filename;
            $answer_obj->user()->associate(Auth::user());
            $answer_obj->questions()->associate($question);
            $answer_obj->save();
        }else{
            $answer_obj = new Answer();
            $answer_obj->answer_text = $answer;
            $answer_obj->user()->associate(Auth::user());
            $answer_obj->questions()->associate($question);
            $answer_obj->save();
        }

        return Redirect::back();

    }

    /**
     * Vote
     */
    public function vote(Request $request, $id)
    {
        $answer = Answer::findOrFail($id);
        $answer->votes += 1;
        $answer->save();
        return Redirect::back();
    }

    /**
     * Ask question (this is where search module works)
     */
    public function ask()
    {
        return view('ask');
    }
}
