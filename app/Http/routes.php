<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

Route::get('/', 'HomeController@index');

/**
 * Add Question
 */
Route::get('/add', 'HomeController@addView');
Route::post('/add/question', 'HomeController@addQuestion');

/**
 * Answers Viewer
 */
Route::get('/question/{id}', 'HomeController@answerView');
Route::post('/question/addanswer/{id}', 'HomeController@addAnswer');
Route::get('/vote/answer/{id}', 'HomeController@vote');

/**
 * Text Search
 */
Route::get('/ask', 'HomeController@ask');